import NexCore from "nexcore-lib";

export class Utils {

    public static isNullOrWhitespace(arg: string) {
        return !arg || arg.trim().length == 0;
    }

    public static isNullOrEmpty<T>(arg: string | Array<T>) {
        return !arg || arg.length == 0;
    }

    public static isValidUrl(s: string, protocols: string[]) {
        try {
            let url = new URL(s);
            return protocols
                ? url.protocol
                    ? protocols.map(x => `${x.toLowerCase()}:`).includes(url.protocol)
                    : false
                : true;
        } catch (err) {
            return false;
        }
    }

    public static parseBigInt(value: number | string | bigint) {
        try {
            return BigInt(value);
        } catch {
            return false;
        }
    }

    public static calcOutpointHash(txIdem: string, outputIndex: number) {
        let writer = new NexCore.encoding.BufferWriter(null);
        let outpoint = writer.write(Buffer.from(txIdem, 'hex').reverse()).writeUInt32LE(outputIndex).toBuffer();
        return NexCore.crypto.Hash.sha256(outpoint).reverse().toString('hex');
    }

    public static sleep(ms: number) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }
}