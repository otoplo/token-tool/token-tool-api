import EventEmitter from "events";

export class ReentrantLock {

  private locked: boolean;
  private ee: EventEmitter;

  constructor() {
    this.locked = false;
    this.ee = new EventEmitter();
  }

  public lock() {
    let promise = new Promise<boolean>(resolve => {
      if (!this.locked) {
        this.locked = true;
        return resolve(true);
      }

      const tryAcquire = () => {
        if (this.tryLock()) {
          this.ee.removeListener('release', tryAcquire);
          return resolve(true);
        }
      };

      this.ee.on('release', tryAcquire);
    });
    
    return promise;
  }

  public unlock() {
    this.locked = false;
    setImmediate(() => this.ee.emit('release'));
  }

  public tryLock() {
    if (!this.locked) {
      this.locked = true;
      return true;
    }
    return false;
  }
}