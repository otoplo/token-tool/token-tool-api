import { CreateTokenDto } from "src/dtos/create-token.dto";
import { Utils } from "src/utils/utils";
import NexCore from "nexcore-lib";
import _ from "lodash";

export class TokenHelper {

  public static readonly PAYMENT_AMOUNT: number = 2000000;

  private static readonly MAX_INT64: bigint = 9223372036854775807n;

  public static validateTokenRequest(createTokenDto: CreateTokenDto): string {
    if (!_.isString(createTokenDto.name) || Utils.isNullOrWhitespace(createTokenDto.name) || createTokenDto.name.length > 20) {
      return "Name should be less than 20 characters";
    }
    if (!_.isString(createTokenDto.ticker) || Utils.isNullOrWhitespace(createTokenDto.ticker) || createTokenDto.ticker.length > 8) {
      return "Ticker should be less than 9 characters";
    }
    if (!Utils.isNullOrEmpty(createTokenDto.docUrl) && !Utils.isValidUrl(createTokenDto.docUrl, ['http', 'https'])) {
      return "Invalid Json URL";
    }
    if (!Number.isInteger(createTokenDto.decimals) || createTokenDto.decimals < 0 || createTokenDto.decimals > 18) {
      return "Decimals must be between 0 and 18";
    }
    if (!_.isBoolean(createTokenDto.lockAmount)) {
      return "Invalid lockAmount value";
    }
    if (!_.isString(createTokenDto.quantity)) {
      return "Quantity cannot be parsed";
    } else {
      let quantity = Utils.parseBigInt(createTokenDto.quantity);
      if (quantity === false || quantity <= 0n) {
        return "Quantity should be whole number and bigger than 0";
      }
      if (quantity * (10n ** BigInt(createTokenDto.decimals)) > this.MAX_INT64) {
        return "Quantity raw value (include decimals) exceeded the max amount - 9223372036854775807";
      }
    }
    if (!this.isValidNexaAddress(createTokenDto.destAddress)) {
      return "Invalid destination address";
    }

    return "";
  }

  public static calcJsonDocHash(jsonDoc: string) {
    let json = jsonDoc.substring(jsonDoc.indexOf('{'), jsonDoc.lastIndexOf('}') + 1);
    return NexCore.crypto.Hash.sha256(Buffer.from(json)).toString('hex');
  }
  
  public static validateJsonSignature(json: string, address: string, signature: string = "") {
    try {
      return NexCore.GroupToken.verifyJsonDoc(json, address, signature);
    } catch {
      return false;
    }
  }

  public static isValidNexaAddress(address: string) {
    try {
      return NexCore.Address.isValid(address) && address.startsWith("nexa:");
    } catch {
      return false;
    }
  }
}