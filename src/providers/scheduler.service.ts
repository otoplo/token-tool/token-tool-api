import { Injectable, Logger } from "@nestjs/common";
import { Cron, CronExpression } from "@nestjs/schedule";
import { DBService } from "./db.service";
import { TokenTxState, TokenTxStatus } from "src/entities/token-tx-state.entity";
import { TokenService } from "./token.service";
import { RostrumService } from "./rostrum.service";
import { Utils } from "src/utils/utils";
import { InputUTXO, UnspentUTXO } from "src/interfaces/utxo.interface";
import { WalletService } from "./wallet.service";
import { DerivePath } from "src/helpers/wallet.helper";
import NexCore from "nexcore-lib";
import { TokenHelper } from "src/helpers/token.helper";

@Injectable()
export class SchedulerService {

  private readonly logger = new Logger(SchedulerService.name);

  private isRunning: boolean;

  constructor(private dbService: DBService, private tokenService: TokenService, private rostrumService: RostrumService, private walletService: WalletService) {
    this.isRunning = false;
  }

  @Cron(CronExpression.EVERY_10_SECONDS)
  async mintingJob() { 
    if (this.isRunning) {
      return;
    }

    try {
      this.isRunning = true;
      await this.createAndMintTokens();
    } catch (e) {
      this.logger.error(`Failed to process minting ${e}`)
    } finally {
      this.isRunning = false;
    }
  }

  private async createAndMintTokens() {
    let states = await this.dbService.getTokenTxStates({ where: { status: TokenTxStatus.PENDING }, order: { date: "ASC" } });
    
    if (Utils.isNullOrEmpty(states)) {
      return;
    }

    this.logger.log("Start minting pending tokens");
    this.logger.log(`Tokens to create/mint: ${states.length}`);

    for (let state of states) {
      let date = new Date();
      date.setMinutes(date.getMinutes() - 10);
      if (state.date.getTime() < date.getTime()) {
        this.logger.log(`Not received payment for state: ${state.uuid} for long time - canceled`);
        await this.dbService.updateTokenTxStatus(state.uuid, TokenTxStatus.CANCELED);
        continue;
      }

      let utxos = await this.rostrumService.listUnspent(state.payAddress);

      if (Utils.isNullOrEmpty(utxos)) {
        this.logger.log(`Not received payment yet for state: ${state.uuid} to address: ${state.payAddress}`);
        continue;
      }

      let amount = 0;
      utxos.forEach(u => {
        amount += u.value;
      });

      if (amount < (TokenHelper.PAYMENT_AMOUNT * 100)) {
        this.logger.log(`Not received enough payment for state: ${state.uuid} to address: ${state.payAddress}`);
        continue;
      }

      try {
        await this.createAndMintToken(state, utxos);
      } catch (e) {
        this.logger.error(`Failed to create/mint token with uid: ${state.uuid}. error: ${e}`);
        await this.dbService.updateTokenTxStatus(state.uuid, TokenTxStatus.FAILED);
      }
    }

    this.logger.log("finish job round");
  }

  private async createAndMintToken(state: TokenTxState, utxos: UnspentUTXO[]) {
    this.logger.log(`Start creation of: ${state.uuid}`);
    await this.dbService.updateTokenTxStatus(state.uuid, TokenTxStatus.CREATING);
    let inputs: InputUTXO[] = [];
    utxos.forEach(utxo => {
      inputs.push({
        outpoint_hash: utxo.outpoint_hash,
        address: state.payAddress,
        tx_pos: utxo.tx_pos,
        value: utxo.value,
        privKey: this.walletService.getKeyByAddress(state.payAddress).getPrivateKey()
      });
    });
    
    let tokenKey = await this.tokenService.getUnusedAddressAndKey();

    let jsonSig: string = "";
    if (!Utils.isNullOrEmpty(state.tokenDocUrl)) {
      let json = await this.tokenService.getJsonDoc(state.tokenDocUrl);
      jsonSig = NexCore.GroupToken.signJsonDoc(json.jsonStr, tokenKey.key.getPrivateKey());
    }

    let changeAddr = await this.walletService.getUnusedChangeAddress();

    let createTxRes = await this.tokenService.generataeCreateTokenTransaction(inputs, tokenKey.address, changeAddr, state.tokenTicker, state.tokenName, state.tokenDocUrl, state.tokenDocHash, state.tokenDecimals);
    let txHash = await this.rostrumService.broadcastTransaction(createTxRes.tx.serialize());
    console.log(txHash);

    let txId = createTxRes.tx.id;
    let txIdem = createTxRes.tx.idem;
    let tokenId = new NexCore.Address(createTxRes.tokenIdBuf, NexCore.Networks.defaultNetwork, NexCore.Address.GroupIdAddress);
    await this.dbService.updateTokenTxState({ id: state.id }, { createTxId: txId, createTxIdem: txIdem, tokenDocSignature: jsonSig, tokenId: tokenId.toString() });

    this.logger.log(`Successfuly created token: ${tokenId}`);

    await Utils.sleep(3000);
    
    this.logger.log(`Start minting of: ${state.uuid}`);
    await this.dbService.updateTokenTxStatus(state.uuid, TokenTxStatus.MINTING);

    let mintInputs: InputUTXO[] = [];
    mintInputs.push({
      address: changeAddr,
      outpoint_hash: Utils.calcOutpointHash(txIdem, 2),
      tx_pos: 2,
      value: createTxRes.tx.outputs[2].satoshis,
      privKey: this.walletService.getKeyByAddress(changeAddr, DerivePath.CHANGE).getPrivateKey()
    });
    mintInputs.push({
      address: tokenKey.address,
      outpoint_hash: Utils.calcOutpointHash(txIdem, 1),
      tx_pos: 1,
      value: createTxRes.tx.outputs[1].satoshis,
      privKey: tokenKey.key.getPrivateKey()
    });

    let mintChangeAddr = "nexa:nqtsq5g5s6ydvjm85smtk7zhgmxrcxmcx36797r0f7wt47mv"
    let mintTx = await this.tokenService.generataeMintTokenTransaction(state, mintInputs, createTxRes.tokenIdBuf, mintChangeAddr);
    let mintTxHash = await this.rostrumService.broadcastTransaction(mintTx.serialize());
    console.log(mintTxHash);

    await this.dbService.updateTokenTxState({ id: state.id }, { mintTxId: mintTx.id, mintTxIdem: mintTx.idem, status: TokenTxStatus.DONE });

    this.logger.log(`Successfuly minted: ${state.uuid}, tokenId: ${tokenId.toString()}`);
  }
}