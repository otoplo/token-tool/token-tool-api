import { Injectable, Logger } from "@nestjs/common";
import { RostrumService } from "./rostrum.service";
import { WalletKeys } from "src/interfaces/wallet-keys.interface";
import HDPrivateKey from "nexcore-lib/types/lib/hdprivatekey";
import { DerivePath, WalletHelper } from "src/helpers/wallet.helper";
import { Balance } from "src/interfaces/balance.interface";
import bigDecimal from "js-big-decimal";
import { CacheService } from "./cache.service";
import { DBService } from "./db.service";
import { ReentrantLock } from "src/utils/reentrant-lock";
import { ConfigService } from "@nestjs/config";
import { WalletIndexColumn } from "src/entities/wallet-index.entity";

@Injectable()
export class WalletService {

  private readonly logger = new Logger(WalletService.name);
  private readonly addressLock = new ReentrantLock();
  private readonly balanceLock = new ReentrantLock();

  private readonly accountKey: HDPrivateKey;

  private walletKeys: WalletKeys;

  public constructor(private configService: ConfigService, private rostrumService: RostrumService, private cacheService: CacheService, private dbService: DBService) {
    this.accountKey = WalletHelper.generateAccountKey(configService.get("SEED"));
  }

  public async initKeys() {
    let idx = await this.dbService.getWalletIndexes();

    this.logger.log(`Init wallet keys with indexes: ${idx.receiveIndex} ${idx.changeIndex}`);
    this.walletKeys = WalletHelper.generateKeysAndAddresses(this.accountKey, 0, idx.receiveIndex, 0, idx.changeIndex);
  }

  public getKeyByAddress(address: string, path: DerivePath = DerivePath.RECEIVE) {
    return path === DerivePath.RECEIVE 
      ? this.walletKeys.receiveKeys.find(k => k.address === address)?.key
      : this.walletKeys.changeKeys.find(k => k.address === address)?.key;
  }

  public async getUnusedAddress() {
    try {
      await this.addressLock.lock();
      let addr = this.walletKeys.receiveKeys.at(-1).address;
      let lastIndex = this.walletKeys.receiveKeys.length;
      await this.dbService.incrementWalletIndex(WalletIndexColumn.RECEIVE_INDEX);
      let newKey = WalletHelper.generateKeyAndAddress(this.accountKey, DerivePath.RECEIVE, lastIndex);
      this.walletKeys.receiveKeys.push(newKey);
      return addr;
    } finally {
      this.addressLock.unlock();
    }
  }

  public async getUnusedChangeAddress() {
    try {
      await this.addressLock.lock();
      let addr = this.walletKeys.changeKeys.at(-1).address;
      let lastIndex = this.walletKeys.changeKeys.length;
      await this.dbService.incrementWalletIndex(WalletIndexColumn.CHANGE_INDEX);
      let newKey = WalletHelper.generateKeyAndAddress(this.accountKey, DerivePath.CHANGE, lastIndex);
      this.walletKeys.changeKeys.push(newKey);
      return addr;
    } finally {
      this.addressLock.unlock();
    }
  }

  public async listAddresses() {
    let startTime = Date.now();
    let receives = [], changes = [];
    this.walletKeys.receiveKeys.forEach((k, i) => {
      let b = this.getAddressBalance(k.address, i);
      receives.push(b);
    });
    this.walletKeys.changeKeys.forEach((k, i) => {
      let b = this.getAddressBalance(k.address, i);
      changes.push(b);
    });

    let [receive, change] = await Promise.all([Promise.all(receives), Promise.all(changes)]);
    this.logger.log(`Fetched addresses in ${Date.now() - startTime}ms`);
    return {receive: receive, change: change};
  }

  public async getAddressBalance(address: string, index: number) {
    let balance = await this.rostrumService.getBalance(address);
    return { index: index, address: address, balance: balance };
  }

  public async getWalletBalance(): Promise<Balance> {
    let startTime = Date.now();
    let balance = await this.cacheService.getValue<Balance>('balance');
    if (balance == null) {
      try {
        await this.balanceLock.lock();
        balance = await this.cacheService.getValue<Balance>('balance');
        if (balance == null) {
          balance = await this.fetchWalletBalance();
          await this.cacheService.setValue('balance', balance, 60);
          this.logger.log(`Fetched wallet balance ${JSON.stringify(balance)} in ${Date.now() - startTime}ms`);
        }
      } finally {
        this.balanceLock.unlock();
      }
    }

    return balance;
  }

  private async fetchWalletBalance(): Promise<Balance> {
    let promises: Promise<Balance>[] = [];

    this.walletKeys.receiveKeys.forEach(key => {
      let b = this.rostrumService.getBalance(key.address);
      promises.push(b);
    });
    this.walletKeys.changeKeys.forEach(key => {
      let b = this.rostrumService.getBalance(key.address);
      promises.push(b);
    });

    var balances = await Promise.all(promises);

    let confirmed = new bigDecimal(0), unconfirmed = new bigDecimal(0);
    balances.forEach(b => {
      confirmed = confirmed.add(new bigDecimal(b.confirmed));
      unconfirmed = unconfirmed.add(new bigDecimal(b.unconfirmed));
    });

    return {confirmed: confirmed.getValue(), unconfirmed: unconfirmed.getValue()};
  }
}