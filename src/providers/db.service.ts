import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { TokenTxState, TokenTxStatus } from "src/entities/token-tx-state.entity";
import { WalletIndex, WalletIndexColumn } from "src/entities/wallet-index.entity";
import { FindManyOptions, FindOptionsWhere, Repository } from "typeorm";
import { QueryDeepPartialEntity } from "typeorm/query-builder/QueryPartialEntity";

@Injectable()
export class DBService {
    
  public constructor(
    @InjectRepository(WalletIndex)
    private walletIdxRepository: Repository<WalletIndex>,

    @InjectRepository(TokenTxState)
    private tokenTxStateRepository: Repository<TokenTxState>,
  ) {}

  public async getWalletIndexes() {
    return await this.walletIdxRepository.findOneBy({ id: 1 });
  }

  public async incrementWalletIndex(indexName: WalletIndexColumn) {
    return await this.walletIdxRepository.increment({ id: 1 }, indexName, 1);
  }

  public async saveTokenTxState(txState: TokenTxState) {
    return await this.tokenTxStateRepository.save(txState);
  }

  public async updateTokenTxStatus(uuid: string, status: TokenTxStatus) {
    return await this.tokenTxStateRepository.update({ uuid: uuid }, { status: status });
  }

  public async getTokenTxStates(options?: FindManyOptions<TokenTxState>) {
    return await this.tokenTxStateRepository.find(options);
  }

  public async getTokenTxState(uuid: string) {
    return await this.tokenTxStateRepository.findOneBy({ uuid: uuid });
  }

  public async updateTokenTxState(whereClause: FindOptionsWhere<TokenTxState>, fields: QueryDeepPartialEntity<TokenTxState>) {
    await this.tokenTxStateRepository.update(whereClause, fields);
  }
}