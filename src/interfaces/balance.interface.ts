export interface Balance {
    confirmed: number | string;
    unconfirmed: number | string;
}