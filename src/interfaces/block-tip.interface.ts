export interface BlockTip {
    height: number;
    hex: string;
}