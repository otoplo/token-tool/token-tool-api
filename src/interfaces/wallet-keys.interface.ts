import HDPrivateKey from "nexcore-lib/types/lib/hdprivatekey";

export interface WalletKeys {
    receiveKeys: AddressKey[];
    changeKeys: AddressKey[];
}

export interface WalletIndexes {
    receiveIndex: number;
    changeIndex: number;
}

export interface TxStatus {
    height: number;
}

export interface AddressKey {
    key: HDPrivateKey;
    address: string;
}